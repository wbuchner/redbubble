//
//  BrandsDataSource.h
//  RedBubble
//
//  Created by Wayne Buchner on 8/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SelectedRowDelegate.h"

@interface BrandsDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic)NSArray *data;
@property (weak) id<SelectedRowDelegate>delegate;

- (instancetype)initWithDataSource:(NSArray *)data delegate:(id)delegate;
- (NSArray *)getDataSource;
@end
