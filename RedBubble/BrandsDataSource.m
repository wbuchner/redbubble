//
//  BrandsDataSource.m
//  RedBubble
//
//  Created by Wayne Buchner on 8/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import "BrandsDataSource.h"
#import "LinkCellTableViewCell.h"

@implementation BrandsDataSource

#define kNormalCellHeight 44.0f
#define kIMageCellHeight 62.0f

typedef enum {
    kSectionZero = 0,
    kSectionOne
} Sections;

/**
 *  Brands Data Source
 *
 *  @param data     array of Strings
 *  @param delegate Delegate to respond to Did Select Row click events
 *
 *  @return UITableView DataSource
 */
- (instancetype)initWithDataSource:(NSArray *)data delegate:(id)delegate
{
    self = [super init];
    if (self) {
        _data = data;
        _delegate = delegate;
        
    }
    return self;
}

#pragma -mark TableView Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == kSectionZero ? 1 : [self.data count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section == kSectionZero ? kNormalCellHeight : kIMageCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case kSectionZero:
        {
            LinkCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"navigation"];
            if (cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"LinkCellTableViewCell" owner:nil options:nil] objectAtIndex:0];
            }
            [cell setupWithLabel:@"Back"];
            return cell;
            break;
        }
            
        case kSectionOne:
        {
            LinkCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"navigation"];
            if (cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"LinkCellTableViewCell" owner:nil options:nil] objectAtIndex:0];
            }
            [cell setupWithLabel:[self.data objectAtIndex:indexPath.row]];
            return cell;
            break;
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case kSectionZero:
             [self.delegate didSelectRow:indexPath identifier:kHomeDataSource];
            break;
            
        case kSectionOne:
            [self.delegate didSelectRow:indexPath identifier:kModelsDataSource];
            break;
    }
   
}

#pragma -mark Public Methods
- (NSArray *)getDataSource
{
    return _data;
}
@end
