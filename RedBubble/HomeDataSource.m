//
//  HomeDataSource.m
//  RedBubble
//
//  Created by Wayne Buchner on 8/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import "HomeDataSource.h"
#import "LinkCellTableViewCell.h"
#import "ImageTableViewCell.h"
#define kNormalCellHeight 44.0f
#define kIMageCellHeight 300.0f
@implementation HomeDataSource

typedef enum {
    kSectionZero = 0,
    kSectionOne
} Sections;

/**
 *  Init the Home Data Source
 *
 *  @param data       an array datasource of Work objects
 *  @param delegate   The delegate to respond to DidSelect Row events
 *  @param navigation Home or Model navigation
 *  @param brand      brand to return for logic operations
 *
 *  @return UITableView DataSource
 */
- (instancetype)initWithDataSource:(NSArray *)data delegate:(id)delegate navigation:(NSString *)navigation brand:(NSString *)brand
{
    self = [super init];
    if (self) {
        _data = data;
        _delegate = delegate;
        _navigation = navigation;
        _brand = brand;
    }
    return self;
}

#pragma -mark TableView Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == kSectionZero ? 1 : [self.data count] >= 10 ? 10 : [self.data count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section == kSectionZero ? kNormalCellHeight : kIMageCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case kSectionZero:
        {
            LinkCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"navigation"];
            if (cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"LinkCellTableViewCell" owner:nil options:nil] objectAtIndex:0];
            }
            [cell setupWithLabel:self.navigation];
            return cell;
            break;
        }
            
        case kSectionOne:
        {
            ImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"imageCell"];
            if (cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"ImageTableViewCell" owner:nil options:nil] objectAtIndex:0];
            }
            [cell setupCellWithWork:[self.data objectAtIndex:indexPath.row]];
            return cell;
            break;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.delegate didSelectRow:indexPath identifier:[_navigation isEqualToString:@"Models"] ? kModelsDataSource : kBrandsDataSource];
}

#pragma -mark Public Methods
- (NSArray *)getDataSource
{
    return _data;
}

- (NSString *)getBrand
{
    return _brand;
}
@end
