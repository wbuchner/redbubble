//
//  ImageTableViewCell.h
//  RedBubble
//
//  Created by Wayne Buchner on 8/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Work.h"
@interface ImageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;

- (void)setupCellWithWork:(Work *)work;
@end
