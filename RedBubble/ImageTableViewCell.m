//
//  ImageTableViewCell.m
//  RedBubble
//
//  Created by Wayne Buchner on 8/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import "ImageTableViewCell.h"
#import "RBImageClient.h"
@implementation ImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupCellWithWork:(Work *)work
{
    if (!work.medium) {
        [work imageURLforSize:kLazyLoadMedium onCompletion:^(NSURL *link) {
            [[RBImageClient sharedInstance] downloadImageWithURL:link onCompletion:^(UIImage *responseImage) {
                work.medium = responseImage;
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (responseImage) {
                        [self setImageWithWork:work];
                    }else{
                        // set No image thumbnail
                    }
                    
                });
            } onFailure:^(NSString *error) {
                // set No image thumbnail
            }];
        }];
    }else{
        [self setImageWithWork:work];
    }
    
}

- (void)setImageWithWork:(Work *)work
{
    [self.image setImage:work.medium];
}
@end
