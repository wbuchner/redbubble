//
//  LinkCellTableViewCell.h
//  RedBubble
//
//  Created by Wayne Buchner on 8/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Work.h"
@interface LinkCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;

- (void)setupWithLabel:(NSString *)text;
@end
