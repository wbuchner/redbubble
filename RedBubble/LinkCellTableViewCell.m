//
//  LinkCellTableViewCell.m
//  RedBubble
//
//  Created by Wayne Buchner on 8/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import "LinkCellTableViewCell.h"

@implementation LinkCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupWithLabel:(NSString *)text
{
    [self.title setText:text];
}

@end
