//
//  ModelsDataSource.h
//  RedBubble
//
//  Created by Wayne Buchner on 8/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SelectedRowDelegate.h"

@interface ModelsDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic)NSArray *data;
@property (weak) id<SelectedRowDelegate>delegate;
@property (strong, nonatomic) NSString *brand;

- (instancetype)initWithDataSource:(NSArray *)data delegate:(id)delegate brand:(NSString *)brand;
- (NSArray *)getDataSource;
- (NSString *)getBrand;

@end
