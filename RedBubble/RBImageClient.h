//
//  RBImageClient.h
//  RedBubble
//
//  Created by Wayne Buchner on 5/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RBImageClient : NSObject

+ (RBImageClient *)sharedInstance;

- (NSURL *)configuredURL;

- (void)loadDataonCompletion:(void(^)(NSDictionary *response))completion onFailure:(void(^)(NSError *error))failure;
- (void)downloadImageWithURL:(NSURL *)url onCompletion:(void(^)(UIImage *responseImage))completion onFailure:(void(^)(NSString *error))failure;
- (NSArray *)getDatasetOfWorksForBrand:(NSString *)brand;
- (void)createBrandLookupTable;
- (NSArray *)getListModelsForBrand:(NSString *)brand;
- (NSArray *)getWorksForBrand:(NSString *)brand andModel:(NSString *)model;

@property (strong, nonatomic)NSArray *works;
@property (strong, nonatomic)NSMutableDictionary *brandLookupTable;
@property (strong, nonatomic)NSMutableArray *brands;

@end
