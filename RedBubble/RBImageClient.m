//
//  RBImageClient.m
//  RedBubble
//
//  Created by Wayne Buchner on 5/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import "RBImageClient.h"
#import "WorkParser.h"
#import "Work.h"

@implementation RBImageClient

+ (RBImageClient *)sharedInstance
{
    static RBImageClient *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] init];
    });
    
    return _sharedClient;
}

/**
 *  Loads data with a completion block
 *
 *  @param completion Returns the Parsed Response Dictionary
 *  @param failure    Returns an error
 */
- (void)loadDataonCompletion:(void(^)(NSDictionary *response))completion onFailure:(void(^)(NSError *error))failure
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[self configuredURL]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if (!error) {
                                          NSDictionary *jsonResponse = [self _handleSuccess:data];
                                          if (jsonResponse) {
                                              // Parse the response
                                              [self _parseWorkResponse:jsonResponse];
                                              
                                              // Create  list of Camera Brands
                                              [self _getBrandList];
                                          }else{
                                              failure(error);
                                          }
                                          completion(jsonResponse);
                                          
                                      }else{
                                          failure(error);
                                      }
                                  }];
    [task resume];

}

/**
 *  Parses the response from the network call
 *
 *  @param data response JSON data
 *
 *  @return Parsed NSDictionary
 */
- (NSDictionary *)_handleSuccess:(NSData *)data
{
    NSError *errorJson=nil;
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
}

/**
 *  Creates a data source of Work objects and stores it in the client.
 *
 *  @param response dictionary
 */
- (void)_parseWorkResponse:(NSDictionary *)response
{
    [WorkParser parseWorks:response onCompletion:^(NSArray *works) {
        self.works = [[NSArray alloc] initWithArray:works];
    }];
}

/**
 *  Cnfigures the URL and its path components from the Configuration.plist
 *
 *  @return NSURL
 */
- (NSURL *)configuredURL
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Configuration" ofType:@"plist"];
    NSDictionary *urlConfiguration = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@%@%@%@%@",
                                 urlConfiguration[@"url.predomain"],
                                 urlConfiguration[@"url.base"],
                                 urlConfiguration[@"url.server"],
                                 urlConfiguration[@"url.version"],
                                 urlConfiguration[@"url.endpoint"]
                                 ]
            ];
}

/**
 *  Downloads an Image with a given URL
 *
 *  @param url        Image URL
 *  @param completion downloaded image
 *  @param failure    error
 */
- (void)downloadImageWithURL:(NSURL *)url onCompletion:(void(^)(UIImage *responseImage))completion onFailure:(void(^)(NSString *error))failure
{
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDownloadTask *getImage = [session downloadTaskWithURL:url
     
               completionHandler:^(NSURL *location,
                                   NSURLResponse *response,
                                   NSError *error) {
                
                   UIImage *responseImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:location]];
                
                   dispatch_async(dispatch_get_main_queue(), ^{
                       if (responseImage) {
                           completion(responseImage);
                       }else{
                           failure(@"No Image");
                       }
                       
                   });
               }];

    [getImage resume];
}

/**
 *  Creates a list of camera brands
 */
- (void)_getBrandList
{
    _brands = [[NSMutableArray alloc] init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (Work *work in _works) {
            if (work.exifData[@"make"]) {
                if (![_brands containsObject:work.exifData[@"make"]]) {
                    [_brands addObject:work.exifData[@"make"]];
                }
            }
        }
        [self createBrandLookupTable];
    });

}

/**
 *  Creates a Hash Table for fast camera works/brand lookup
 */
- (void)createBrandLookupTable
{
    // create a hash map of camera Brands and ID's so lookup can be faster
    _brandLookupTable = [[NSMutableDictionary alloc] init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *worksCopy = [NSMutableArray arrayWithArray:_works];
        for (NSString  *brand in _brands) {
            // look through all
            NSMutableArray *idents = [NSMutableArray new];
            NSMutableArray *removeObjetcs = [NSMutableArray new];
            for (Work *work in worksCopy) {
                if (work.exifData[@"make"] && [work.exifData[@"make"] isEqualToString:brand]) {
                    // add id of the work object to an array
                    [idents addObject:work.identification];
                    
                    // temp add the object to remove from the copy array so we don't need to chec it next time through. Good for large data sets
                    [removeObjetcs addObject:work];
                }
            }
            // Remove the identified Work objectd from the copy array so we don't iterate over them again.
            [worksCopy removeObjectsInArray:removeObjetcs];
            [_brandLookupTable setValue:idents forKey:[NSString stringWithFormat:@"%@",brand]];
        }
        NSLog(@"_brandLookupTable: %@",_brandLookupTable);
    });
}

/**
 *  Returns a data set Array of all WORK objects for a brand
 *
 *  @param brand NSString Camera brand
 *  @param model model
 *
 *  @return NSArray of Work Objects
 */
- (NSArray *)getWorksForBrand:(NSString *)brand andModel:(NSString *)model
{
    NSArray *works = [self.brandLookupTable valueForKey:brand];
    NSMutableArray *result = [NSMutableArray new];
    for (NSNumber *identification in works) {
        Work *work = [self _getWorkWithID:identification];
        if ([work.exifData[@"model"] isEqualToString:model]) {
            [result addObject:work];
        }
    }
    return result;
}

/**
 *  Returns all Works for a Brans
 *
 *  @param brand NSSring Camera brand
 *
 *  @return NSArray data set of Work objects
 */
- (NSArray *)getDatasetOfWorksForBrand:(NSString *)brand
{
    NSMutableArray *worksDataSource = [NSMutableArray new];
    NSMutableArray *worksCopy = [NSMutableArray arrayWithArray:_works];
    
    for (NSNumber *identification in [_brandLookupTable valueForKey:brand]) {
        for (Work *work in worksCopy) {
            if ([work.identification isEqualToNumber:identification]) {
                [worksDataSource addObject:work];
            }
        }
        // Remove the identified Work objectd from the copy array so we don't iterate over them again. Good for large data sets
        [worksCopy removeObjectsInArray:worksDataSource];
    }
    worksCopy = nil;
    return worksDataSource;
}

/**
 *  Returns a list of all Models for a brand
 *
 *  @param brand NSString Camera Brand
 *
 *  @return NSArray data set of Work objects
 */
- (NSArray *)getListModelsForBrand:(NSString *)brand
{
    NSMutableArray *worksDataSource = [NSMutableArray new];
    for (Work *work in _works) {
        if ([work.exifData[@"make"] isEqualToString:brand]) {
            [worksDataSource addObject:work.exifData[@"model"]];
        }
    }
    return [NSArray arrayWithArray:worksDataSource];
}

/**
 *  Returns a Work object with a given Identification
 *
 *  @param identification NSNumber
 *
 *  @return Work object
 */
- (Work *)_getWorkWithID:(NSNumber *)identification
{
    Work *result;
    for (Work *work in _works) {
        if ([work.identification isEqualToNumber:identification]) {
            result = work;
            
        }
    }
    return result;
}
@end
