//
//  ViewController.h
//  RedBubble
//
//  Created by Wayne Buchner on 5/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeDataSource.h"
#import "BrandsDataSource.h"
#import "ModelsDataSource.h"
#import "SelectedRowDelegate.h"
@interface RBMainViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, SelectedRowDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (strong, nonatomic) id dataSource;
@end

