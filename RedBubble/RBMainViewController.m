//
//  ViewController.m
//  RedBubble
//
//  Created by Wayne Buchner on 5/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

/*
 
 This is  the main view controller. It received a new datasource with data based on selected rows.
 
 */
#import "RBMainViewController.h"
#import "RBImageClient.h"
@interface RBMainViewController ()

@end

@implementation RBMainViewController
@synthesize delegate;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [[RBImageClient sharedInstance] loadDataonCompletion:^(NSDictionary *response) {

        dispatch_async(dispatch_get_main_queue(), ^{
            _loadingLabel.text = @"Complete";
            // create the initial Data Source
            self.dataSource = [[HomeDataSource alloc] initWithDataSource:[RBImageClient sharedInstance].works delegate:self navigation:@"Brands" brand:@""];
            [self.tableView reloadData];
        });
    } onFailure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to retrieve Images" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:^{ }];
        });
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (self.dataSource) return [self.dataSource numberOfSectionsInTableView:tableView];
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.dataSource) return [self.dataSource tableView:tableView numberOfRowsInSection:section];
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataSource) return [self.dataSource tableView:tableView heightForRowAtIndexPath:indexPath];
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataSource) return [self.dataSource tableView:tableView cellForRowAtIndexPath:indexPath];
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataSource) return [self.dataSource tableView:tableView didSelectRowAtIndexPath:indexPath];
}

/**
 *  SelectedRowDelegate delegate callback
 *
 *  @param indexPath  Index path fo the selected Row
 *  @param identifier Identifier for the navgation
 */
- (void)didSelectRow:(NSIndexPath *)indexPath identifier:(NSUInteger)identifier
{
    switch (indexPath.section) {
        case 0:
        {
            switch (identifier) {
                case kHomeDataSource:
                {
                    self.dataSource = [[HomeDataSource alloc] initWithDataSource:[RBImageClient sharedInstance].works delegate:self navigation:@"Brands" brand:@""];
                    if (self.dataSource)[self.tableView reloadData];
                    break;
                }
                case kBrandsDataSource:
                {
                    self.dataSource = [[BrandsDataSource alloc] initWithDataSource:[RBImageClient sharedInstance].brands delegate:self];
                    if (self.dataSource)[self.tableView reloadData];
                    break;
                }
                    
                case kModelsDataSource:
                {
                    NSArray *modelsDataset = [[RBImageClient sharedInstance] getListModelsForBrand:[self.dataSource getBrand]];
                    self.dataSource = [[ModelsDataSource alloc] initWithDataSource:modelsDataset delegate:self brand:[self.dataSource getBrand]];
                    if (self.dataSource)[self.tableView reloadData];
                    break;
                }
                    
                default:
                    
                    break;
            }
            break;
        }
            
        case 1:
        {
            switch (identifier) {
                case kModelsDataSource:
                {
                    // get all models for the brand
                    NSArray *modelsDataset = [[RBImageClient sharedInstance] getListModelsForBrand:[[self.dataSource getDataSource] objectAtIndex:indexPath.row]];
                    self.dataSource = [[ModelsDataSource alloc] initWithDataSource:modelsDataset delegate:self brand:[[self.dataSource getDataSource] objectAtIndex:indexPath.row]];
                    if (self.dataSource)[self.tableView reloadData];
                    break;
                }
                case kDetailDisclosure:
                {
                    NSArray *worksArray = [[RBImageClient sharedInstance] getWorksForBrand:[self.dataSource getBrand] andModel:[[self.dataSource getDataSource] objectAtIndex:indexPath.row]];
                    self.dataSource = [[HomeDataSource alloc] initWithDataSource:worksArray delegate:self navigation:@"Models" brand:[self.dataSource getBrand]];
                    if (self.dataSource)[self.tableView reloadData];
                    break;
                }
                default:
                    break;
            }
            break;
        }
    }
}

@end
