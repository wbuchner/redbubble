//
//  SelectedRowDelegate.h
//  RedBubble
//
//  Created by Wayne Buchner on 8/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kHomeDataSource = 0,
    kBrandsDataSource,
    kModelsDataSource,
    kDetailDisclosure
} Identifier;

@protocol SelectedRowDelegate <NSObject>
@property (weak) id<SelectedRowDelegate>delegate;
- (void)didSelectRow:(NSIndexPath *)indexPath identifier:(NSUInteger)identifier;
@end
