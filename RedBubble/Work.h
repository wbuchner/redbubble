//
//  Work.h
//  RedBubble
//
//  Created by Wayne Buchner on 5/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Work : NSObject

/**
 Enumerated type of Image sizes
 */
typedef enum {
    kLazyLoadSmall = 0,
    kLazyLoadMedium,
    kLazyLoadLarge
} ImageSize;

/**
 *  Dictionary containing the Image Exif data
 */
@property (strong, nonatomic)NSDictionary *exifData;

/**
 *  Filename of the Work object
 */
@property (strong, nonatomic)NSString *fileName;

/**
 *  Identification of the Work object
 */
@property (strong, nonatomic)NSNumber *identification;

/**
 *  Array of Dictionaries containing URLS indexed by size
 */
@property (strong, nonatomic)NSMutableArray *urls;

/**
 *  Images the woker object
 */
@property (strong, nonatomic)UIImage *small;
@property (strong, nonatomic)UIImage *medium;
@property (strong, nonatomic)UIImage *large;

/**
 *  Worker object init method
 *
 *  @param dictionary Data source.
 *
 *  @return worker object
 */
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;


- (void)_getUrlForSize:(NSString *)size onCompletion:(void(^)(NSURL *link))completion;
- (void)imageURLforSize:(NSUInteger)size onCompletion:(void(^)(NSURL *link))completion;

@end
