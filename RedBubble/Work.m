//
//  Work.m
//  RedBubble
//
//  Created by Wayne Buchner on 5/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import "Work.h"

@implementation Work

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        [self constructWork:dictionary];
        
    }
    return self;
}

/**
 *  Constructs a Work object
 *
 *  @param work NSDictionary a dictionary to init the object
 */
- (void)constructWork:(NSDictionary *)work
{
    _exifData = [work valueForKey:@"exif"] ? work[@"exif"] : @{@"exif":@"no exif data available"};
    _fileName = [work valueForKey:@"filename"] ? work[@"filename"] : @"no filename";
    _identification = [work valueForKey:@"id"] ? [NSNumber numberWithInteger:[work[@"id"] integerValue]] : nil;
    _urls = [NSMutableArray new];
    for (NSDictionary *url in work[@"urls"][@"url"])
    {
        [self.urls addObject:[self _urlBuilder:url]];
    }
    
}

/**
 *  Builds a dictionary for storage
 *
 *  @param urlDictionary NSDictionary
 *
 *  @return NSDictionary
 */
- (NSDictionary *)_urlBuilder:(NSDictionary *)urlDictionary
{
    NSString *size = [urlDictionary valueForKey:@"type"] ? urlDictionary[@"type"] : @"";
    NSString *link = [urlDictionary valueForKey:@"link"] ? urlDictionary[@"link"] : @"";
    
    return @{size : link};
}

/**
 *  Retrieves an NSURL from the objects array of Dictionaries
 *  containing link strings in the completion block based on the size argument.
 *
 *  @param size
 *  @param completion returns the full NSURL string
 */
- (void)_getUrlForSize:(NSString *)size onCompletion:(void(^)(NSURL *link))completion
{
    __block NSURL *result;
    [_urls enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *links = (NSDictionary *)obj;
        result = [[links.allKeys firstObject] isEqualToString:size] ? [NSURL URLWithString:links[ [links.allKeys firstObject] ]] : nil;
        if (result) {
            *stop = result ? YES : NO;
            completion(result);
        }
        
    }];
    
}

/**
 *  Returns a full NSURL to the caller based on the size
 *
 *  @param size       Worker class ImageSize enumerated type
 *  @param completion returns the URL in the completion block.
 */
- (void)imageURLforSize:(NSUInteger)size onCompletion:(void(^)(NSURL *link))completion
{
    NSString *imageSize;
    switch (size) {
        case kLazyLoadSmall:
            imageSize = @"small";
            break;
            
        case kLazyLoadMedium:
            imageSize = @"medium";
            break;
        case kLazyLoadLarge:
            imageSize = @"large";
            break;
    }
    
    [self _getUrlForSize:imageSize onCompletion:^(NSURL *link) {
        completion(link);
    }];
}
@end
