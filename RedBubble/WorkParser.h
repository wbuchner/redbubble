//
//  WorkParser.h
//  RedBubble
//
//  Created by Wayne Buchner on 7/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorkParser : NSObject

+ (void)parseWorks:(NSDictionary *)response onCompletion:(void(^)(NSArray *works))completion;
@end
