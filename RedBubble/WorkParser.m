//
//  WorkParser.m
//  RedBubble
//
//  Created by Wayne Buchner on 7/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import "WorkParser.h"
#import "Work.h"
@implementation WorkParser

/**
 *  Builds a Worker objects from a Dictionary Data Source
 *
 *  @param response   the data source
 *  @param completion Array of Worker objects
 */
+ (void)parseWorks:(NSDictionary *)response onCompletion:(void(^)(NSArray *works))completion
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSMutableArray *works = [NSMutableArray new];
        for (NSDictionary *work in response[@"works"][@"work"]) {
            [works addObject:[[Work alloc] initWithDictionary:work]];
        }
        completion([NSArray arrayWithArray:works]);
    });
}
@end
