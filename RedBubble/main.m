//
//  main.m
//  RedBubble
//
//  Created by Wayne Buchner on 5/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
