//
//  RedBubbleTests.m
//  RedBubbleTests
//
//  Created by Wayne Buchner on 5/05/2016.
//  Copyright © 2016 Wayne Buchner. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RBImageClient.h"
#import <ImageIO/ImageIO.h>
#import "Work.h"

@interface RedBubbleTests : XCTestCase

@end

@implementation RedBubbleTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testConfigureURL {
    // This test should build a url configured from the Configuration.plist
    NSURL *URL = [NSURL URLWithString:@"http://take-home-test.herokuapp.com/api/v1/works.json"];
    
    NSURL *testURL = [[RBImageClient sharedInstance] configuredURL];
    XCTAssertTrue([[testURL absoluteString] isEqualToString:[URL absoluteString]]);
}

- (void)testLoadData {
    // Test the asynchronous call for data
    XCTestExpectation *expectation = [self expectationWithDescription:@"Testing Async Load Data from Client"];
    
    [[RBImageClient sharedInstance] loadDataonCompletion:^(NSDictionary *response)
    {
        XCTAssert(response);
        [expectation fulfill];
    } onFailure:^(NSError *error) {
        XCTFail(@"request failed: %@",error);
    }];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        
        if(error)
        {
            XCTFail(@"Expectation Failed with error: %@", error);
        }else{
            
        }
    }];
}

- (void)testParseWorkClass {
    // Test creation of a Work Class
    XCTestExpectation *expectation = [self expectationWithDescription:@"Testing Async Load Data from Client"];
    
    [[RBImageClient sharedInstance] loadDataonCompletion:^(NSDictionary *response) {
        NSDictionary * work = response[@"works"][@"work"][0];
        Work *workObject = [[Work alloc] initWithDictionary:work];
        XCTAssertNotNil(workObject);
        XCTAssertNotNil(workObject.identification);
        XCTAssertNotNil(workObject.fileName);
        XCTAssertNotNil(workObject.exifData);
        XCTAssertNotNil(workObject.urls);
        

        [expectation fulfill];
    } onFailure:^(NSError *error) {
        XCTFail(@"request failed: %@",error);
    }];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        
        if(error)
        {
            XCTFail(@"Expectation Failed with error: %@", error);
        }else{
            
        }
    }];
}

- (void)testImageURLCreationFromEnumeratedArgument
{
    // Test creation of the URL from a given Enumerated Type (size)
    // Test Size 135x135 == small
    // Test Size 300x300 == medium
    // Test Size 550x550 == large
    XCTestExpectation *expectation = [self expectationWithDescription:@"Test creation of the URL from a given Enumerated Type (size)"];
    
    [[RBImageClient sharedInstance] loadDataonCompletion:^(NSDictionary *response) {
        NSDictionary * work = response[@"works"][@"work"][0];
        Work *workObject = [[Work alloc] initWithDictionary:work];
        
        [workObject imageURLforSize:kLazyLoadMedium onCompletion:^(NSURL *link) {
            NSArray *urlComponenets = [link.absoluteString componentsSeparatedByString:@","];
            XCTAssertTrue([urlComponenets[1] isEqualToString:@"300x300"]);
            NSLog(@"link: %@", urlComponenets);
            [expectation fulfill];
        }];
        
    } onFailure:^(NSError *error) {
        XCTFail(@"request failed: %@",error);
    }];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        
        if(error)
        {
            XCTFail(@"Expectation Failed with error: %@", error);
        }else{
            
        }
    }];

}

- (void)testDownloadImage
{
    // Test downloading an image from a URL
    XCTestExpectation *expectation = [self expectationWithDescription:@"Test downloading an image from a URL"];
   
    NSURL *url = [NSURL URLWithString:@"http://ih1.redbubble.net/work.31820.1.flat,135x135,075,f.jpg"];
    
    [[RBImageClient sharedInstance] downloadImageWithURL:url onCompletion:^(UIImage *responseImage) {
        UIImage *image =  responseImage;
        XCTAssertNotNil(image);
        [expectation fulfill];
    } onFailure:^(NSString *error) {
        XCTFail(@"request failed: %@",error);
    }];
    
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        
        if(error)
        {
            XCTFail(@"Expectation Failed with error: %@", error);
        }else{
            
        }
    }];
}
@end
